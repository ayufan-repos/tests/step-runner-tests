package main

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type Context struct {
	ID     string
	Path   string
	Env    map[string]string
	Inputs map[string]string
}

func NewContext(path string) *Context {
	return &Context{
		Path:   path,
		Env:    map[string]string{},
		Inputs: map[string]string{},
	}
}

func (c *Context) Mutate() *Context {
	env := make(map[string]string, len(c.Env))
	for k, v := range c.Env {
		env[k] = v
	}

	return &Context{
		Env:    env,
		Inputs: map[string]string{},
	}
}

func (c *Context) ReplaceValueInput(value string) string {
	for k, v := range c.Inputs {
		replaceString := fmt.Sprintf("${{inputs.%s}}", k)
		value = strings.ReplaceAll(value, replaceString, v)
	}
	return value
}

func (c *Context) ReplaceArrayInputs(values []string) []string {
	newValues := make([]string, 0)
	for _, value := range values {
		newValues = append(newValues, c.ReplaceValueInput(value))
	}
	return newValues
}

func (c *Context) ReplaceMapInputs(values map[string]string) map[string]string {
	newValues := map[string]string{}

	for k, value := range values {
		newValues[k] = c.ReplaceValueInput(value)
	}
	return newValues
}

type StepSpecInput map[string]*string // nil - means required, non-nil means optional
type StepEnvironment map[string]string

type StepSpec struct {
	Inputs StepSpecInput `yaml:"inputs"`
}

type Runner interface {
	Run(context *Context) error
}

type StepDefinition struct {
	Path    string      `yaml:"-"`
	Spec    StepSpec    `yaml:"spec"`
	Content StepContent `yaml:"content"`
}

type StepContent struct {
	Env      StepEnvironment       `yaml:"env"`
	Sequence *StepContentSequence  `yaml:"steps"`
	Exec     *StepContentExec      `yaml:"exec"`
	Step     *StepContentReference `yaml:"step"`
}

type StepContentSequence []StepContent

type StepContentExec struct {
	Command []string `yaml:"command"`
}

type StepInputValue map[string]string

type StepContentReference struct {
	Reference string         `yaml:"reference"`
	Inputs    StepInputValue `yaml:"inputs"`
}

func LoadYAML(path string, subject interface{}) error {
	println("Reading", path, "...")
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return fmt.Errorf("yamlFile.Get err #%v", err)
	}

	err = yaml.Unmarshal(content, subject)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	return err
}

func LoadStepDefinition(path string) (*StepDefinition, error) {
	sd := &StepDefinition{Path: path}
	return sd, LoadYAML(path, sd)
}

func (p *StepContent) Run(context *Context) error {
	context.Env["SCRIPT_PATH"] = context.Path

	runners := []Runner{}

	if p.Exec != nil {
		runners = append(runners, p.Exec)
	}
	if p.Sequence != nil {
		runners = append(runners, p.Sequence)
	}
	if p.Step != nil {
		runners = append(runners, p.Step)
	}

	if len(runners) != 1 {
		return fmt.Errorf("%q cannot infer type of execution. There has to be exactly one of exec, steps or step. There is %d.",
			context.Path, len(runners))
	}

	return runners[0].Run(context)
}

func (e *StepContentExec) Run(context *Context) error {
	cmdArgs := context.ReplaceArrayInputs(e.Command)
	cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)
	cmd.Env = os.Environ()
	for k, v := range context.ReplaceMapInputs(context.Env) {
		cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", k, v))
	}
	cmd.Env = append(cmd.Env, fmt.Sprintf("SCRIPT_STEP_ID=%s", context.ID))
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func (e *StepContentSequence) Run(context *Context) error {
	for i, p := range *e {
		newContext := context.Mutate()
		newContext.Inputs = context.Inputs
		newContext.ID = context.ID + "=>" + fmt.Sprintf("seq_%d", i)

		err := p.Run(newContext)
		if err != nil {
			return err
		}
	}
	return nil
}

func (e *StepContentReference) Run(context *Context) error {
	sd, err := LoadStepDefinition(e.Reference)
	if err != nil {
		return err
	}

	newContext := context.Mutate()
	newContext.ID = context.ID
	newContext.Inputs = context.ReplaceMapInputs(e.Inputs)

	return sd.Run(newContext)
}

func (e *StepDefinition) Run(context *Context) error {
	newContext := context.Mutate()
	newContext.Path = e.Path
	newContext.ID = context.ID + "=>" + filepath.Base(e.Path)

	// Fill context with inputs
	for k, specValue := range e.Spec.Inputs {
		userValue, ok := context.Inputs[k]
		if ok {
			newContext.Inputs[k] = userValue
		} else if specValue == nil {
			return fmt.Errorf("Input %s is required, but undefined in %q.", k, newContext.Path)
		} else {
			newContext.Inputs[k] = *specValue
		}
	}

	return e.Content.Run(newContext)
}

func main() {
	content := &StepContent{}
	err := LoadYAML("gitlab-ci-steps.yml", content)
	if err != nil {
		panic(err)
	}

	topContext := NewContext("gitlab-ci-steps.yml")
	topContext.ID = "root"
	err = content.Run(topContext)
	if err != nil {
		println("Execution Error", err.Error())
	} else {
		println("Done.")
	}
}
