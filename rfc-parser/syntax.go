package main

////////////////////////////////////////
// Parsing Data Structures
////////////////////////////////////////

type StepEnvironment map[string]string

type StepSpecInput struct {
	Default *string `yaml:"default"`
}

type StepSpecInputs map[string]StepSpecInput

type StepSpec struct {
	Inputs StepSpecInputs `yaml:"inputs"`
}

type StepSpecDoc struct {
	Spec StepSpec `yaml:"spec"`
}

type StepType string

const StepTypeExec StepType = "exec"
const StepTypeSteps StepType = "steps"
const StepTypeParallel StepType = "parallel"

type StepDefinition struct {
	StepSpecDoc `yaml:"-"`
	Path        string `yaml:"-"`

	Type     StepType                `yaml:"type"`
	Steps    *StepDefinitionSteps    `yaml:"steps"`
	Exec     *StepDefinitionExec     `yaml:"exec"`
	Parallel *StepDefinitionParallel `yaml:"parallel"`
	Env      StepEnvironment         `yaml:"env"`
}

type StepDefinitionExec struct {
	Command []string `yaml:"command"`
}

type StepDefinitionSteps []StepReference
type StepDefinitionParallel []StepReference

type StepReferenceInputs map[string]string

type StepReference struct {
	Step   string              `yaml:"step"`
	Inputs StepReferenceInputs `yaml:"inputs"`
	Env    StepEnvironment     `yaml:"env"`
}
