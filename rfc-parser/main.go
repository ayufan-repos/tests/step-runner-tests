package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
)

////////////////////////////////////////
// RFC implementation
////////////////////////////////////////

type StepRunner interface {
	Run(context *Context) error
}

func helperToJson(subject interface{}) string {
	data, _ := json.Marshal(subject)
	return string(data)
}

type Context struct {
	IDs    []string
	Env    map[string]string
	Inputs map[string]string
	Dir    string
}

func NewContext(dir string, id ...string) *Context {
	return &Context{
		IDs:    id,
		Dir:    dir,
		Env:    map[string]string{},
		Inputs: map[string]string{},
	}
}

func (c *Context) GetID() string {
	return strings.Join(c.IDs, "=>")
}

func (c *Context) Mutate(id ...string) *Context {
	env := make(map[string]string, len(c.Env))
	for k, v := range c.Env {
		env[k] = v
	}

	return &Context{
		IDs:    append(c.IDs, id...),
		Dir:    c.Dir,
		Env:    env,
		Inputs: map[string]string{},
	}
}

func (c *Context) Clone(id ...string) *Context {
	return &Context{
		IDs:    append(c.IDs, id...),
		Dir:    c.Dir,
		Env:    c.Env,
		Inputs: c.Inputs,
	}
}

func (c *Context) ReplaceValueInput(value string) string {
	for k, v := range c.Inputs {
		replaceString := fmt.Sprintf("${{inputs.%s}}", k)
		value = strings.ReplaceAll(value, replaceString, v)
	}
	return value
}

func (c *Context) ReplaceArrayInputs(values []string) []string {
	newValues := make([]string, 0)
	for _, value := range values {
		newValues = append(newValues, c.ReplaceValueInput(value))
	}
	return newValues
}

func (c *Context) ReplaceMapInputs(values map[string]string) map[string]string {
	newValues := map[string]string{}

	for k, value := range values {
		newValues[k] = c.ReplaceValueInput(value)
	}
	return newValues
}

func LoadYAML(path string, subjects ...interface{}) error {
	println("Reading", path, "...")
	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("yamlFile.Get err #%v", err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)
	d.KnownFields(true)
	for _, subject := range subjects {
		err = d.Decode(subject)
		if err != nil {
			return fmt.Errorf("%q: Unmarshal: %v", path, err)
		}
	}

	return nil
}

func LoadStepDefinition(path string, dir string) (*StepDefinition, error) {
	if strings.HasPrefix(path, "./") || strings.HasPrefix(path, "../") {
		resolvedPath := filepath.Join(dir, path)
		sd := &StepDefinition{Path: resolvedPath}
		return sd, LoadYAML(resolvedPath, &sd.StepSpecDoc, sd)
	}

	return nil, fmt.Errorf("%q is not supported", path)
}

func (p *StepDefinition) getTypes() map[StepType]StepRunner {
	types := make(map[StepType]StepRunner)
	if p.Exec != nil {
		types[StepTypeExec] = p.Exec
	}
	if p.Steps != nil {
		types[StepTypeSteps] = p.Steps
	}
	if p.Parallel != nil {
		types[StepTypeParallel] = p.Parallel
	}
	return types
}

func (p *StepDefinition) getType() (StepRunner, error) {
	types := p.getTypes()

	if len(types) == 0 {
		return nil, errors.New("no type defined")
	} else if len(types) > 1 {
		return nil, fmt.Errorf("There has to be exactly one of exec or steps. There is %d.", len(types))
	} else if types[p.Type] == nil {
		return nil, fmt.Errorf("No runner for type %q", p.Type)
	} else {
		return types[p.Type], nil
	}
}

func (p *StepDefinition) Run(context *Context, inputs map[string]string) error {
	newContext := context.Mutate(filepath.Base(p.Path))
	newContext.Dir = filepath.Dir(p.Path)
	newContext.Env["SCRIPT_PATH"] = p.Path

	runnerType, err := p.getType()
	if err != nil {
		return fmt.Errorf("%s: %q", newContext.GetID(), err)
	}

	// Fill context with inputs
	for inputName, specValue := range p.Spec.Inputs {
		userValue, ok := inputs[inputName]
		if ok {
			newContext.Inputs[inputName] = userValue
		} else if specValue.Default == nil {
			return fmt.Errorf("Input %s is required, but undefined in %q.", inputName, p.Path)
		} else {
			newContext.Inputs[inputName] = *specValue.Default
		}
	}

	// Expand env variables
	for k, v := range newContext.ReplaceMapInputs(p.Env) {
		newContext.Env[k] = v
	}

	return runnerType.Run(newContext)
}

func (e *StepDefinitionExec) Run(context *Context) error {
	cmdArgs := context.ReplaceArrayInputs(e.Command)
	cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)
	cmd.Env = os.Environ()
	for k, v := range context.ReplaceMapInputs(context.Env) {
		cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", k, v))
	}
	cmd.Env = append(cmd.Env, fmt.Sprintf("SCRIPT_STEP_ID=%s", context.GetID()))
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Dir = context.Dir
	return cmd.Run()
}

func (e *StepDefinitionSteps) Run(context *Context) error {
	for i, p := range *e {
		newContext := context.Clone(fmt.Sprintf("seq_%d", i))

		err := p.Run(newContext)
		if err != nil {
			return err
		}
	}
	return nil
}

func (e *StepDefinitionParallel) Run(context *Context) error {
	var wg sync.WaitGroup
	var lock sync.Mutex
	var resultErr error

	for i, p := range *e {
		wg.Add(1)

		i := i
		p := p
		go func() {
			defer wg.Done()

			newContext := context.Clone(fmt.Sprintf("seq_%d", i))

			err := p.Run(newContext)

			lock.Lock()
			defer lock.Unlock()
			if err != nil {
				resultErr = err
			}
		}()
	}

	wg.Wait()
	return resultErr
}

func (e *StepReference) Run(context *Context) error {
	if e.Step == "" {
		return fmt.Errorf("%s invalid Step Reference", context.GetID())
	}

	sd, err := LoadStepDefinition(e.Step, context.Dir)
	if err != nil {
		return err
	}

	return sd.Run(context, context.ReplaceMapInputs(e.Inputs))
}

func main() {
	Steps := &StepDefinitionSteps{}
	err := LoadYAML("gitlab-ci-steps.yml", Steps)
	if err != nil {
		panic(err)
	}

	topContext := NewContext(".", "root")
	err = Steps.Run(topContext)
	if err != nil {
		println("Execution Error", err.Error())
	} else {
		println("Done.")
	}
}
