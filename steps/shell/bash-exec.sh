#!/usr/bin/env /bin/bash

set -xeo pipefail

# move to job directory
cd "$JOB_DIR"

# execute job script
eval "$JOB_SCRIPT"
