module gitlab.com/ayufan-repos/tests/step-runner-tests

go 1.20

require (
	github.com/db47h/ragel/v2 v2.2.4 // indirect
	golang.org/x/tools v0.14.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
