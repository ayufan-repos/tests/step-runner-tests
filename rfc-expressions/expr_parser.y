%{
package main
%}

%union {
   id string
   number int64
   str string
   expr *Expression
   exprList []*Expression
}

%token <id> ID
%token <number> NUMBER
%token <str> STRING
%token NUMBER EQUAL NOT_EQUAL ID DOT STRING OPEN CLOSE AND OR SEPARATOR

%type <expr> start expression or_expression and_expression comparison_expression value_expression
%type <exprList> expression_list

%%

start: expression {
  exprlex.(*exprLexerImpl).result = $1;
};

expression_list:
    expression_list SEPARATOR expression { $$ = append($1, $3); }
  | expression { $$ = []*Expression{ $1 }; }

expression: or_expression { $$ = $1; }

or_expression:
    or_expression OR and_expression { $$ = &Expression{Op: Expression_or, LeftExpr: $1, RightExpr: $3}; }
  | and_expression { $$ = $1; }

and_expression:
    and_expression AND comparison_expression { $$ = &Expression{Op: Expression_and, LeftExpr: $1, RightExpr: $3}; }
  | comparison_expression { $$ = $1; }

comparison_expression:
    comparison_expression NOT_EQUAL value_expression { $$ = &Expression{Op: Expression_not_equal, LeftExpr: $1, RightExpr: $3}; }
  | comparison_expression EQUAL value_expression { $$ = &Expression{Op: Expression_equal, LeftExpr: $1, RightExpr: $3}; }
  | value_expression { $$ = $1; }

value_expression:
    ID { $$ = &Expression{Op: Expression_variable, VariableValue: $1}; }
  | NUMBER { $$ = &Expression{Op: Expression_number, NumberValue: $1}; }
  | STRING { $$ = &Expression{Op: Expression_string, StringValue: $1}; }
  | OPEN expression CLOSE { $$ = $2; }
  | value_expression DOT ID { $$ = &Expression{Op: Expression_access, AccessExpr: $1, VariableValue: $3}; }
  | ID OPEN expression_list CLOSE { $$ = &Expression{Op: Expression_call, FuncID: $1, FuncExprs: $3}; }
  | ID OPEN CLOSE { $$ = &Expression{Op: Expression_call, FuncID: $1}; }
