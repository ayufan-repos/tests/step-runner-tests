package main

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type Value interface{}
type ArrayValue []Value
type HashValue map[string]Value

func Dig(v Value, name string) Value {
	switch v.(type) {
	case HashValue:
		return v.(HashValue)[name]
	default:
		return fmt.Errorf("Dig: Not supported: %q", v)
	}
}

func ToString(v Value) (string, error) {
	switch v.(type) {
	case int64:
		return strconv.FormatInt(v.(int64), 10), nil
	case bool:
		if v.(bool) {
			return "true", nil
		}
		return "false", nil
	case string:
		return v.(string), nil
	case ArrayValue, HashValue:
		{
			data, _ := json.Marshal(v)
			return string(data), nil
		}
	case error:
		return "", v.(error)
	default:
		return "", fmt.Errorf("IsTrue: Not supported: %q", v)
	}
}

func Equals(value, otherValue Value) Value {
	value1String, value1Err := ToString(value)
	value2String, value2Err := ToString(otherValue)
	if value1Err != nil && value2Err != nil {
		return fmt.Errorf("Many errors: %q, %q", value1Err, value2Err)
	} else if value1Err != nil {
		return value1Err
	} else if value2Err != nil {
		return value2Err
	} else {
		return value1String == value2String
	}
}

func IsTrue(v Value) bool {
	switch v.(type) {
	case int64:
		return v.(int64) != 0
	case bool:
		return v.(bool)
	case string:
		return v.(string) != ""
	case ArrayValue:
		return len(v.(ArrayValue)) > 0
	case HashValue:
		return len(v.(HashValue)) > 0
	case nil:
		return false
	default:
		fmt.Println("IsTrue: Not supported: %q", v)
		return false
	}
}

func IsNull(v Value) bool {
	switch v {
	case nil:
		return true
	default:
		return false
	}
}

func IsError(v Value) bool {
	switch v.(type) {
	case error:
		return true
	default:
		return false
	}
}

func (e *Expression) Calculate(context *Context) Value {
	switch e.Op {
	case Expression_unknown:
		return nil
	case Expression_equal:
		return Equals(e.LeftExpr.Calculate(context), e.RightExpr.Calculate(context))
	case Expression_not_equal:
		{
			expr := Equals(e.LeftExpr.Calculate(context), e.RightExpr.Calculate(context))
			if IsError(expr) {
				return expr
			}
			return !IsTrue(expr)
		}
	case Expression_and:
		{
			expr := e.LeftExpr.Calculate(context)
			if IsError(expr) || !IsTrue(expr) {
				return expr
			}
			return e.RightExpr.Calculate(context)
		}
	case Expression_or:
		{
			expr := e.LeftExpr.Calculate(context)
			if IsError(expr) || IsTrue(expr) {
				return expr
			}
			return e.RightExpr.Calculate(context)
		}
	case Expression_access:
		return Dig(e.AccessExpr.Calculate(context), e.VariableValue)
	case Expression_not:
		{
			expr := e.UnaryExpr.Calculate(context)
			if IsError(expr) {
				return expr
			}
			return !IsTrue(expr)
		}
	case Expression_string:
		return InterpolateString(e.StringValue, context)
	case Expression_number:
		return e.NumberValue
	case Expression_variable:
		value := context.Globals[e.VariableValue]
		if IsNull(value) {
			return fmt.Errorf("%q not found", e.VariableValue)
		}
		return value
	case Expression_call:
		{
			values := ArrayValue{}
			for _, funcExpr := range e.FuncExprs {
				value := funcExpr.Calculate(context)
				if IsError(value) {
					return value
				}
				values = append(values, value)
			}
			return context.Call(e.FuncID, values)
		}
	default:
		fmt.Println("%q not supported", e.Op)
		return nil
	}
}

func Parse(text string) (*Expression, error) {
	lexer := newExprLexer([]byte(text))
	status := exprParse(lexer)
	if status != 0 {
		return nil, fmt.Errorf("Parse failure: %d", status)
	}

	return lexer.result, nil
}

func RunString(text string, context *Context) Value {
	expr, err := Parse(text)
	if err != nil {
		return err
	}
	return expr.Calculate(context)
}

const InterpolateOpen = "${{"
const InterpolateClose = "}}"

var interpolateRegex = regexp.MustCompile(regexp.QuoteMeta(InterpolateOpen) + "|" + regexp.QuoteMeta(InterpolateClose))

func InterpolateString(text string, context *Context) Value {
	output := []string{}
	depth := 0
	prev_idx := 0
	open_idx := 0

	for _, loc := range interpolateRegex.FindAllStringIndex(text, -1) {
		if depth == 0 {
			output = append(output, text[prev_idx:loc[0]])
		}
		prev_idx = loc[1]

		switch text[loc[0]:loc[1]] {
		case InterpolateOpen:
			depth += 1

			if depth == 1 {
				open_idx = loc[1]
			}
			break

		case InterpolateClose:
			depth -= 1

			if depth < 0 {
				return fmt.Errorf("Unaligned ${{ ... }}")
			} else if depth > 0 {
				break
			}

			insideString := text[open_idx:loc[0]]
			insideValue := RunString(insideString, context)
			insideValueString, err := ToString(insideValue)
			if err != nil {
				return err
			}
			output = append(output, insideValueString)
			break

		default:
		}
	}

	if depth > 0 {
		return fmt.Errorf("The is not closed: ${{ ... }}")
	}

	output = append(output, text[prev_idx:])
	return strings.Join(output, "")
}
