package main

import "fmt"

type Context struct {
	Globals HashValue
}

func (c *Context) callAdd(values ArrayValue) Value {
	var sum int64
	for _, value := range values {
		if i, ok := value.(int64); ok {
			sum += i
		} else {
			return fmt.Errorf("%q is not a number", value)
		}
	}
	return sum
}

func (c *Context) callConcat(values ArrayValue) Value {
	s := ""
	for _, value := range values {
		ss, err := ToString(value)
		if err != nil {
			return err
		}
		s += ss
	}
	return s
}

func (c *Context) Call(id string, values ArrayValue) Value {
	switch id {
	case "add":
		return c.callAdd(values)
	case "concat":
		return c.callConcat(values)
	default:
		return fmt.Errorf("Function %q not found", id)
	}
}
