#!/bin/bash

set -xeo pipefail
go generate
go fmt .
exec go run *.go
