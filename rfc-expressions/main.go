package main

import (
	"encoding/json"
	"fmt"
)

//go:generate ragel -Z -G2 -o expr_lexer.gen.go expr_lexer.rl
//go:generate goyacc -p expr -o expr_parser.gen.go expr_parser.y
//go:generate protoc proto.proto --go_out=./

func testExpression(text string, context *Context) {
	fmt.Println("===================")
	fmt.Println("EXPR:", text)
	fmt.Println("===================")

	expression, err := Parse(text)
	if err != nil {
		fmt.Println("ERR:", err)
		return
	}

	data, err := json.MarshalIndent(expression, "", "    ")
	if err != nil {
		fmt.Println("ERR:", err)
		return
	}

	fmt.Println(string(data))
	fmt.Println("===================")

	value := expression.Calculate(context)
	valueString, valueErr := ToString(value)
	if valueErr == nil {
		fmt.Println("CALC:", valueString)
	} else {
		fmt.Println("CALC ERR:", valueErr)
	}
}

func main() {
	exprErrorVerbose = true

	context := &Context{
		Globals: HashValue{
			"aaa": int64(20),
			"bbb": int64(30),
			"runtime": HashValue{
				"OS": "linux",
			},
			"inputs": HashValue{
				"shell": "bash",
			},
		},
	}
	testExpression(`runtime.OS`, context)
	testExpression(`runtime.OS == "windows"`, context)
	testExpression(`runtime.OS == "windows" && inputs.shell == "detect" || inputs.shell == "bash"`, context)
	testExpression(`(aaa == 10) == bbb`, context)
	testExpression(`"windows ${{ inputs.shell }}"`, context)
	testExpression(`"windows \"${{ \"nested ${{ inputs.shell }}\" }}\""`, context)
	testExpression(`add(aaa, bbb)`, context)
	testExpression(`concat(aaa, bbb, "aaa", inputs.shell == "detect")`, context)
	testExpression(`concat()`, context)
}
